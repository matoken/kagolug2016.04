#!/bin/bash

ASCIIDOC="asciidoc"
WKHTMLTOPDF="$HOME/usr/local/wkhtmltox/bin/wkhtmltopdf"

for f in *.adoc
do
  $ASCIIDOC --backend slidy $f
  echo "mv ${f/\.adoc/\.html} ${f/\.adoc/_slide\.html}"
  mv ${f/\.adoc/\.html} ${f/\.adoc/_slide\.html}
  $ASCIIDOC $f
  $WKHTMLTOPDF ${f/\.adoc/\.html} ${f/\.adoc/\.pdf}
done


