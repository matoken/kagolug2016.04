= 鹿児島Linux勉強会 2016.04

:author:   https://kagolug.org/[鹿児島LinuxUsersGroup]
:backend:   slidy
:max-width: 45em
:data-uri:
:icons:

* http://kagolug.connpass.com/event/29919/[鹿児島Linux勉強会 2016.04 - connpass]
* 2016-04-15(Sat) codeArts株式会社 会議室

== 会場提供

* http://www.codearts.co.jp/[codeArts株式会社様]
* まさくらさん

ありがとうございます!

== 注意事項等

* 飲食
** OK
* 電源/Wi-FI
** Wi-Fi(まさくらさん提供)_ +
essid : ---------- +
key : ----------
* TitanPadにメモを残せるようにしてあります．
** https://kagolug.titanpad.com/3
* Twitter
** https://twitter.com/search?f=tweets&q=%23kagolug&src=typd[#kagolug] 皆がつぶやいてくれるとTogetterにまとめたり出来て嬉しい +
前回 -> http://togetter.com/li/949724[鹿児島Linux勉強会 2016.03の記録 - Togetterまとめ]
*** 遅れてくる人のTweetに気づいた時は教えてください https://twitter.com/search?f=tweets&q=%23kagolug&src=typd[#kagolug]

== 鹿児島らぐ https://kagolug.org/

* 2014-04-26 〜
* Linux勉強会は0~14 で15回め
* Linux/PC-UNIX/OSSなどの話題で盛り上がる場にしたい


== 今日の参加者

== matoken (matoken)

* この勉強会を何で知りましたか?
** 脳内
* 発表や相談したいことがある人は良かったら書いてください
** ちょっと古いマシンにUbunuMATE入れた話とか?

== Tomo Masakura (masakura)

* この勉強会を何で知りましたか?
** メーリングリスト
* 発表や相談したいことがある人は良かったら書いてください
** (回答なし)

== saekishinya (saekishinya)※キャンセル

* この勉強会を何で知りましたか?
** 懇親会
* 発表や相談したいことがある人は良かったら書いてください
** (回答なし)

== AkitakaShiogai (AkitakaShiogai)

* この勉強会を何で知りましたか?
** 前回のイベント
* 発表や相談したいことがある人は良かったら書いてください
** USB LiveLinux環境を作った話

== Susumu Odahara (odasusu)

* この勉強会を何で知りましたか?
** メーリングリスト
* 発表や相談したいことがある人は良かったら書いてください
** LinuxでMS SQL

== pengin333 (pengin333)

* この勉強会を何で知りましたか?
** インターネッツ
* 発表や相談したいことがある人は良かったら書いてください
** これからやりたいこと

== kuma35 (kuma35)

* この勉強会を何で知りましたか?
** MATOKENさんと調整した。
* 発表や相談したいことがある人は良かったら書いてください
** emacsのtrampとかどうよ？、ARMじゃない腕ロボット、Ubuntu MATE on Rspi2とかとか。

== 最近のあれこれ

* HackCafe_Kanoya
** 初めて2人!
** 出来れば定期開催したい……
** 鹿児島市でもぜひ
* @kagolug_ml
** MLに投稿があったらTweetするだけのbot(Lingrのbotと同じ動き)
** 一度アカウントロックされてしまいましたが復旧して稼働中
* @kagolug も一応作った
* kagolug.org ドメイン更新
** 来年の今くらいまでは使えます

== 次回以降

* 今のところ未定
* 誰か主催しませんか?
* 歴史的に一人でやってるとその人が居なくなったり興味を失うとコミニュティは消えます><
* やること
** 会場の手配
*** 集まってワイワイやってても怒られないところ
*** 理想は電源/Wi-Fi/プロジェクタ完備だけど無くても大丈夫
** 告知，募集
*** http://connpass.com/ +
2回以上参加したことがある人には管理者権限付与したつもり
*** ML +
http://list.kagolug.org/cgi-bin/mailman/listinfo/users
*** Web
https://kagolug.org/ の下のログインからアカウント登録して編集 +
wysiwygでhtmlとか解らなくても大丈夫な感じになっています
** 当日の進行
* 必ずしもこれまでのやりかたを踏襲する必要はない

== 大隅での開催?

* Linux遣いで鹿屋の会場を確保してくれるという方が居る
** リナシティかのや
** 鹿児島中央駅からバスで行ける
** 1日6往復
** 片道\1,400-
*** http://www.e-kanoya.net/htmbox/kikaku/20091201_tyokkou.html[鹿屋市（かのや市）|鹿児島中央駅－鹿屋間直行バス]
* 垂水
** 前回の懇親会で垂水案が出た
** 市外の人間でも借りれるかなど会場確認は未だ……
